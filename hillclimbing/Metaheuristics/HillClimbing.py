from .Metaheuristic import Metaheuristic
from Operators import tweak, generate_solution, calculate_fitness


class HillClimbing(Metaheuristic):
    def __init__(self, problem, fit, iterations=1000):
        self.iterations = iterations
        self.fit = fit
        super().__init__(problem)

    def run(self):
        # print('\nhill climbing')
        # self.random.seed(65)
        self.solution = generate_solution(self.problem, self.random)
        # print(self.solution)
        self.problem.fitness = calculate_fitness(
            self.problem, list(self.solution))
        i = 0
        while i < self.iterations:
            R = tweak(list(self.solution), self.random, self.fit, self.problem)
            fitness_r = calculate_fitness(self.problem, list(R))

            if fitness_r < self.problem.fitness:
                self.solution = R
                self.problem.fitness = fitness_r
            # print('fitness [', i+1, ']', self.problem.fitness)
            i += 1
        # print('iterations',i)
        # print('best solution',self.solution)
        # print('fitness', self.problem.fitness)
        return self.problem.fitness
