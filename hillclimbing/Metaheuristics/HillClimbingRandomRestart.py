from .Metaheuristic import Metaheuristic
from Operators import tweak, generate_solution, calculate_fitness


class RandomRestart(Metaheuristic):
    def __init__(self, problem, fit, time, iterations=1000):
        self.iterations = iterations
        self.fit = fit
        self.time = time
        super().__init__(problem)

    def run(self):
        '''
        run the hill climbing random restarts
        '''
        # print('\nRandom Restarts')
        # self.random.seed(65)
        self.solution = generate_solution(self.problem, self.random)
        solution_fitness = calculate_fitness(
            self.problem, list(self.solution))
        self.best = self.solution
        best_fitness = solution_fitness
        i = 0
        while i < self.iterations:
            time = self.random.randint(1, self.time)
            j = 0
            while j <(time-1) and i < self.iterations:
                """
                1. generate r
                2. generate r's fitness
                3. evaluate r's fitness and solution's fitness
                """
                r = tweak(list(self.solution), self.random,
                          self.fit, self.problem)

                fitness_r = calculate_fitness(self.problem, list(r))
                if fitness_r < solution_fitness:
                    self.solution = r
                    solution_fitness = fitness_r
                
                # number of evaluations
                i += 1
                j += 1
            if best_fitness > solution_fitness:
                self.best = self.solution
                best_fitness = solution_fitness

            self.solution = generate_solution(self.problem, self.random)
            solution_fitness = calculate_fitness(
                self.problem, list(self.solution))
            # i += 1
        # print('iterations',i)
        # print('best solution', self.best)
        # print('fitness', best_fitness)
        return best_fitness
