from .Metaheuristic import Metaheuristic
from Operators import tweak, generate_solution, calculate_fitness
from tools.init import initRepeat
import math


class IteratedLocalSearch(Metaheuristic):
    def __init__(self, problem, fit, iterations=1000):
        self.iterations = iterations
        self.fit = fit
        self.solution  = initRepeat(list, self.random.randint,args=(0,1), n = problem.length )
        super().__init__(problem)

    def run(self):
        T = 2 # todo
        self.solution = self.random
        while True:

