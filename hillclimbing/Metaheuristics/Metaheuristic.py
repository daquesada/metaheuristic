from abc import ABC, abstractmethod
import random

class Metaheuristic(ABC):
    def __init__(self, problem):
        self.random = random
        self.solution = 0
        self.best = 0
        self.problem = problem

    @abstractmethod
    def run(self):
        pass
