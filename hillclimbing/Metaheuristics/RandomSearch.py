from .Metaheuristic import Metaheuristic
from Operators import tweak, generate_solution, calculate_fitness


class RandomSearch(Metaheuristic):
    def __init__(self, problem, iterations=1000):
        self.iterations = iterations
        super().__init__(problem)

    def run(self):
        # print('\nRandom Search')
        # self.random.seed(65)
        self.best = generate_solution(self.problem, self.random)
        best_fitness = calculate_fitness(self.problem, self.best)
        i = 0
        while i < self.iterations:
            self.solution = generate_solution(self.problem, self.random)
            solution_fitness = calculate_fitness(self.problem, self.solution)
            if solution_fitness < best_fitness:
                best_fitness = solution_fitness
                self.best = self.solution
            i += 1
        # print('iterations',i)
        # print('best solution', self.best)
        # print('fitness', best_fitness)
        return best_fitness
