from .Metaheuristic import Metaheuristic
from Operators import tweak, generate_solution, calculate_fitness
import math


class SimulatedAnnealing(Metaheuristic):
    def __init__(self, problem, fit, temperature, iterations=1000):
        self.iterations = iterations
        self.fit = fit
        self.temperature = temperature
        super().__init__(problem)

    def run(self):
        # print('\nSimulated Annealing')
        # self.random.seed(65)
        temp = self.temperature
        self.solution = generate_solution(self.problem, self.random)
        solution_fitness = calculate_fitness(self.problem, list(self.solution))
        best_fitness = solution_fitness
        self.best = self.solution
        i = 0
        while i < self.iterations:
            r = tweak(list(self.solution), self.random, self.fit, self.problem)

            r_fitness = calculate_fitness(self.problem, list(r))
            random_number = self.random.random()
            probability = self.acceptance_probability(
                r_fitness, solution_fitness, temp)

            if r_fitness < solution_fitness or random_number < probability:
                self.solution = r
                solution_fitness = r_fitness

            if solution_fitness < best_fitness:
                self.best = self.solution
                best_fitness = solution_fitness
            i += 1
        # print('iterations',i)
        # print('best solution', self.best)
        # print('fitness', best_fitness)
        return best_fitness

    def acceptance_probability(self, r_fitness, solution_fitness, temp):
        """
        calculate the accptance probability from the exponetial function
        """
        p = math.exp((solution_fitness-r_fitness)/temp)
        if (p > 1):
            p = 1
        return p
