from .Metaheuristic import Metaheuristic
from Operators import tweak, generate_solution, calculate_fitness


class SteepestAscent(Metaheuristic):
    def __init__(self, problem, fit, n_tweaks, iterations=1000):
        self.iterations = iterations
        self.fit = fit
        self.n_tweaks = n_tweaks
        self.maxEFO = 0
        super().__init__(problem)

    def run(self):
        # print('\nSteepest Ascent')
        # self.random.seed(65)
        self.solution = generate_solution(self.problem, self.random)
        solution_fitness = calculate_fitness(self.problem, list(self.solution))
        i = 0
        while i < self.iterations:
            r = tweak(list(self.solution), self.random, self.fit, self.problem)
            r_fitness = calculate_fitness(self.problem, list(r))
            j = 0
            while j < self.n_tweaks-1 and i < self.iterations:
                w = tweak(list(self.solution), self.random,
                          self.fit, self.problem)
                w_fitness = calculate_fitness(self.problem, list(w))
                if w_fitness < r_fitness:
                    r = w
                    r_fitness = w_fitness
                j += 1
                i += 1
            if r_fitness < solution_fitness:
                self.solution = r
                solution_fitness = r_fitness
        # print('number of iterations',i)
        # print('best solution', self.solution)
        # print('fitness', solution_fitness)
        return solution_fitness
