def tweak(solution, random, fit, problem):
    """
    returns a solution
    """
    for i in range(len(solution)):
        # solution[i]+=r
        r = random.uniform(problem.lower_limit, problem.upper_limit)
        solution[i] = solution[i] + (-1 * fit + 2 * fit*r)
        
        if solution[i] > problem.upper_limit:
            solution[i] = problem.upper_limit
        elif solution[i] < problem.lower_limit:
            solution[i] = problem.lower_limit
        
    return solution

def tweak_binary(solution, random,p=0.3):
    """
    returns a solution
    """
    for i in range(len(solution)):
        if random.random() < p:
            solution[i] = int(not solution[i])   
    return solution
    # [ 1 0 0 1  1 0 1 0]
    # [ 0 0 0 1  0 1 0 1]

def generate_solution(problem, random):
    """
    return a list full of random numbers between a limits
    """
    

    
    return [random.uniform(problem.lower_limit, problem.upper_limit) for _ in range(problem.length)]
def initIterate(function):


def calculate_fitness(problem, solution):
    """
    calulate the fitness from determinate problem
    """
    return problem.evaluation(solution)
