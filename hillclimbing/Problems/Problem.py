from abc import ABC, abstractmethod


class Problem(ABC):
    def __init__(self, length, lower_limit, upper_limit, fitness=0):
        self.length = length
        self.lower_limit = lower_limit
        self.upper_limit = upper_limit
        self.fitness = fitness

    @abstractmethod
    def evaluation(self, solution):
        pass
    
    @abstractmethod
    def to_string(self):
        pass