from .Problem import Problem
import math


class Rastrigin(Problem):
    def __init__(self, length, lower_limit=-5.12, upper_limit=5.12, fitness=0):
        super().__init__(length, lower_limit, upper_limit, fitness)

    def evaluation(self, solution):
        n = len(solution)
        evaluation = 0
        pi = math.pi
        for i in range(n):
            evaluation += abs(solution[i]*solution[i] - 10 *
                              math.cos(2*pi*solution[i]) + 10)
        return evaluation

    def to_string(self):
        return 'Rastrigin'
