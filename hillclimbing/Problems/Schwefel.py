from .Problem import Problem
import math


class Schwefel(Problem):
    def __init__(self, length, lower_limit, upper_limit, fitness=0):
        super().__init__(length, lower_limit, upper_limit, fitness)

    def evaluation(self, solution):
        n = len(solution)
        evaluation = 0
        for i in range(n):
            for j in range(i):
                evaluation += solution[j]**2
        return evaluation
            

    def to_string(self):
        return 'Schwefel'