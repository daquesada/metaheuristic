from .Problem import Problem
import math


class Step(Problem):
    def __init__(self, length, lower_limit, upper_limit, fitness=0):
        super().__init__(length, lower_limit, upper_limit, fitness)

    def evaluation(self, solution):
        evaluation = 0
        for i in range(self.length):
            evaluation += math.pow((abs(solution[i]+0.5)), 2)
        return evaluation
    
    def to_string(self):
        return 'Step'