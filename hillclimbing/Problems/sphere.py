from .Problem import Problem

class Sphere(Problem):
    def __init__(self, length, lower_limit, upper_limit, fitness=0):
        super().__init__(length, lower_limit, upper_limit, fitness)
    
    def evaluation(self, solution):
        evaluation = 0
        for i in range(self.length):
            evaluation += solution[i]*solution[i] 
        return evaluation 

    def to_string(self):
        return 'Sphere'