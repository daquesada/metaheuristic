"""
@author: Daniel Felipe Quesada Gomez
"""


from Metaheuristics.HillClimbing import HillClimbing
from Metaheuristics.HillClimbingRandomRestart import RandomRestart
from Metaheuristics.RandomSearch import RandomSearch
from Metaheuristics.SimulatedAnnealing import SimulatedAnnealing
from Metaheuristics.SteepestAscent import SteepestAscent

from Problems.sphere import Sphere
from Problems.Step import Step
from Problems.Schwefel import Schwefel
from Problems.Rastrigin import Rastrigin

from utils import init_excel, create_file, close_file, write_excel, add_average


def main():

    #settings

    lower_limit = -100
    upper_limit = 100
    length = 2
    fit = 0.01
    time = 1000
    temperature = 1.0
    n_tweaks = 4
    n_iterations = 31

    #problems

    s = Sphere(length, lower_limit, upper_limit)
    step = Step(length, lower_limit, upper_limit)
    schwefel = Schwefel(length, lower_limit, upper_limit)
    rastrigin = Rastrigin(length)
    problems = [s, step, schwefel, rastrigin]

    try:
        workbook = create_file()
        for problem in problems:
            i = 0
            worksheet = init_excel(problem.to_string(), workbook)
            while i < n_iterations:
                #metaheuristics            
                hc = HillClimbing(problem, fit)
                rr = RandomRestart(problem, fit, time)
                rs = RandomSearch(problem)
                SA = SimulatedAnnealing(problem, fit, temperature)
                sahc = SteepestAscent(problem, fit, n_tweaks)

                #run metaheuristic

                write_excel(hc.run(), i+1, 1, worksheet)
                write_excel(rr.run(), i+1, 2, worksheet)
                write_excel(rs.run(), i+1, 3, worksheet)
                write_excel(SA.run(), i+1, 4, worksheet)
                write_excel(sahc.run(), i+1, 5, worksheet)
                i += 1
            print('==============================================\n')
            print(problem.to_string(),'Done\n')
            print('==============================================\n')
            add_average(worksheet, i)
        close_file(workbook)
        print('check out stats.xlsx file')
    except:
        print('\nstats.xlsx file open or fit is too high, try again')
    

main()
