import random

#z = y^2 + x^2

solution = []
v_max = 2
li = -0.001
ls = 0
random.seed(1)
def generate_solution(length=v_max):
    return [random.uniform(li, ls) for _ in range(length)]


def tweak(solution):
    r_solution = solution
    for i in range(len(solution)):
        ran = random.uniform(li, ls)
        r_solution[i] += ran
    return r_solution


def evaluation(solution):
    fs = 0
    for i in range(len(solution)):
        fs += solution[i]*solution[i]
    return fs


i = 0
solution = generate_solution()
while i < 100:
    r = tweak(list(solution))
    if evaluation(solution) > evaluation(r):
        solution = r
    print(evaluation(solution))
    i += 1
