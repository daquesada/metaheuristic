def initRepeat(container, func,args, n):
    return container(func(*args) for _ in xrange(n))

def initIterate(container, generator):
 
    return container(generator())

def initCycle(container, seq_func, n=1):

    return container(func() for _ in xrange(n) for func in seq_func)

