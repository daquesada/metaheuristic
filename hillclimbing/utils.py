import xlsxwriter

def create_file():
    workbook = xlsxwriter.Workbook('stats.xlsx')
    return workbook

def init_excel(worksheet_name, workbook):
    worksheet = workbook.add_worksheet(name=worksheet_name)
    worksheet.write(0,1,"Hill Climbing")
    worksheet.write(0,2,"Random Restart")
    worksheet.write(0,3,"Random Search")
    worksheet.write(0,4,"Simulated Annealing")
    worksheet.write(0,5,"Steepest Ascent")
    return worksheet

def add_average(worksheet, iterations=31):
    row = iterations + 2    
    worksheet.write(row, 0, 'average')
    worksheet.write(row, 1, '=SUM(B2:B'+str(row)+')/'+str(iterations))
    worksheet.write(row, 2, '=SUM(C2:C'+str(row)+')/'+str(iterations))
    worksheet.write(row, 3, '=SUM(D2:D'+str(row)+')/'+str(iterations))
    worksheet.write(row, 4, '=SUM(E2:E'+str(row)+')/'+str(iterations))
    worksheet.write(row, 5, '=SUM(F2:F'+str(row)+')/'+str(iterations))

def close_file(workbook):
    workbook.close()

def write_excel(data, row, col, worksheet):
    worksheet.write(row, col, float(data))
    
